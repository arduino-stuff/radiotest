/**
 *
 * Pins:
 * Hardware SPI:
 * MISO -> 12
 * MOSI -> 11
 * SCK -> 13
 *
 * Configurable:
 * CE -> 8
 * CSN -> 7
 */

#include <SPI.h>
#include <Mirf.h>
#include <nRF24L01.h>
#include <MirfHardwareSpiDriver.h>
#include "pins.h"
#include "lights.h"

Lights lights;
volatile unsigned long int send_value = 0;

void button_press() {
  int state = digitalRead(PIN_BUTTON);
  if (state == LOW) {
    send_value = 8;
    digitalWrite(PIN_LED_RIGHT, HIGH);
  }
  else if (state == HIGH) {
    send_value = 7;
    digitalWrite(PIN_LED_RIGHT, LOW);
  }
}

void setup(){
  Serial.begin(9600);
  /*
   * Setup pins / SPI.
   */

  Mirf.cePin = A0;
  Mirf.csnPin = A1;
  
  Mirf.spi = &MirfHardwareSpi;
  Mirf.init();
  
  /*
   * Configure reciving address.
   */   
  Mirf.setRADDR((byte *)"bjrt1");
  
  /*
   * Set the payload length to sizeof(unsigned long) the
   * return type of millis().
   *
   * NB: payload on client and server must be the same.
   */
   
  Mirf.payload = sizeof(unsigned long);
  
  /*
   * Write channel and payload config then power up reciver.
   */
   
  /*
   * To change channel:
   * 
   * Mirf.channel = 10;
   *
   * NB: Make sure channel is legal in your area.
   */
   
  Mirf.config();
  
  led_cycle(&lights, PIN_LED_LEFT, 250);
  led_cycle(&lights, PIN_LED_RIGHT, 250);
  lights.set(PIN_LED_BOTH, 0, 255, 0);
  digitalWrite(PIN_LED_LEFT, LOW);
  digitalWrite(PIN_LED_RIGHT, LOW);
  pinMode(PIN_BUTTON, INPUT_PULLUP);
  attachInterrupt(INT_BUTTON, button_press, CHANGE);
  
  Serial.println("Beginning ... "); 
}

void loop(){
  Serial.println("LOOP");
  unsigned long test = 0;
  while(!Mirf.dataReady()){
    if (send_value > 0) {    
      Mirf.setTADDR((byte *)"bjrt1");
      Mirf.send((byte *)&send_value);
      while(Mirf.isSending()){
      }
      Serial.println("Finished sending");
      send_value = 0;
    }
  }
  Serial.println("test");
  Mirf.getData((byte *) &test);
  if (test > 0) {
    
    Serial.println(test);
    if (test == 8) {
      digitalWrite(PIN_LED_LEFT, HIGH);
    } else {
      digitalWrite(PIN_LED_LEFT, LOW);
    }
  }
} 
  
  
  
